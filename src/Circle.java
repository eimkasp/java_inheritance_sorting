public class Circle extends Figure {

    double radius;
    double pi = 3.14;

    Circle(double radius) {
        super(); // iskvieciamas Figure klases konstruktorius
        this.radius = radius;
        this.calculareArea();
    }

    double calculareArea() {
       this.area = this.pi * (this.radius * this.radius);
       return this.area;
    }
}
